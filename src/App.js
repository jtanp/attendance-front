import './App.css';
import Attendance from './components/Attendance';

function App() {
  return (
    <div className="App">
      <h2>Attendance</h2>
      <Attendance title="Attendance codes"/>
    </div>
  );
}

export default App;
