import React, { useState, useEffect } from "react";
import ReactTable from "react-table-v6";
import "react-table-v6/react-table.css";

const Attendance = (props) => {
    const [attendance, setAttendance] = useState([]);

    useEffect(() => {
        fetch("https://attendance-e1700807.herokuapp.com/attendances")
        .then(res => {
          if (res.ok) {
            return res.json();
          }
        })
        .then((data) => setAttendance(data))
        .catch((error) => console.log("Fetching attendances failed.", error))
    },[])

    const columns = [
      {
        Header: "Id",
        accessor: "id",
      },
      {
        Header: "Key",
        accessor: "key",
      },
      {
        Header: "Date",
        accessor: "date",
      }
    ];

    return(
        <div>
            <h4>{props.title}</h4>
            <ReactTable data={attendance} columns={columns}/>
        </div>
    )
}

export default Attendance;